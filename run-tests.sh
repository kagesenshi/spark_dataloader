#!/bin/bash

set -e

ORACLE_CONTAINER_EXISTS=`podman ps -a --format "{{.Names}}"|grep oraclexe|wc -l`
if [ "x$ORACLE_CONTAINER_EXISTS" == "x0" ];then
  podman container create -d --rm -p 1521:1521 --name oraclexe -e ORACLE_PASSWORD=password docker.io/gvenzl/oracle-xe:21-slim-faststart
fi

ORACLE_CONTAINER_STARTED=`podman ps --format "{{.Names}}"|grep oraclexe|wc -l`
if [ "x$ORACLE_CONTAINER_STARTED" == "x0" ];then
  podman start oraclexe
  echo "Waiting for oracle container to properly start"
  sleep 240
fi

MSSQL_CONTAINER_EXISTS=`podman ps -a --format "{{.Names}}"|grep mssqlxe|wc -l`
if [ "x$MSSQL_CONTAINER_EXISTS" == "x0" ];then
  podman container create -d --rm -p 1433:1433 --name mssqlxe -e MSSQL_PID=Express -e ACCEPT_EULA=Y -e "MSSQL_SA_PASSWORD=Passw0rd" mcr.microsoft.com/mssql/server:2022-latest
fi

MSSQL_CONTAINER_STARTED=`podman ps --format "{{.Names}}"|grep mssqlxe|wc -l`
if [ "x$MSSQL_CONTAINER_STARTED" == "x0" ];then
  podman start mssqlxe
  echo "Waiting for mssql container to properly start"
  sleep 240
fi



CONTAINER_EXISTS=`toolbox list -c|awk '{print $2}'|grep spark_dataloader|wc -l`

if [ "x$CONTAINER_EXISTS" == "x0" ];then
    podman build -t spark_dataloader .
    toolbox create -i spark_dataloader spark_dataloader
fi
toolbox run -c spark_dataloader /opt/venv/bin/pytest tests
