import sys
import subprocess
import pyspark
from getpass import getuser
from spark_dataloader.rdbms_ingestion import incremental_append_ingestion, Args, conn_from_args
import shutil
import glob
import os
import time

def test_full_ingestion(oraclexe, spark_session):
    spark = spark_session
    raw_dbmeta = spark.sql('describe database default').collect()
    dbmeta = {}
    for i in raw_dbmeta:
        dbmeta[i[0]] = i[1]
    spark.sql('drop table if exists data_append')
    dblocation = dbmeta['Location'][5:]
    if os.path.exists(dblocation):
        shutil.rmtree(dblocation)

    args = Args(
            jdbc='jdbc:oracle:thin:@localhost:1521/XEPDB1',
            driver='oracle.jdbc.driver.OracleDriver',
            username='system',
            password='password',
            dbtable='data_append',
            partition_column='id',
            output_partition='date',
            incremental_column='id',
            storageformat='parquet',
            ingestion_tag_column='dl_ingest_date',
            num_partitions='2')


    # populate database
    cursor = oraclexe.cursor()
    cursor.execute("""
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE data_append';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
                   """)

    cursor.execute('''
        create table data_append(
            id integer, value varchar(30), created timestamp,
            "date" varchar(10))''')

    for i in range(0,10):
        cursor.execute(
                '''insert into data_append(id, created, "date") values (:data_id, current_timestamp,
                to_char(current_timestamp, 'YYYY-MM-DD'))''', [i])
    oraclexe.commit()

    cursor.execute('select count(1) from data_append')
    assert cursor.fetchone()[0] == 10

    conn = conn_from_args(spark_session, args)
    df = conn.load()
    output_partitions = []
    if args.output_partition_columns:
        output_partitions = args.output_partition_columns.split(',')

    db = 'default'
    tbl = 'data_append'

    ingest_count = incremental_append_ingestion(
        spark, df, db, tbl,
            args.incremental_column,
            args.last_value,
            args.storageformat,
            ingestion_tag_column=args.ingestion_tag_column,
            output_partitions=output_partitions)

    assert ingest_count == 10
    assert spark.sql('select count(1) as rowc from data_append').collect()[0].rowc == 10


    for i in range(10,20):
        cursor.execute(
                '''insert into data_append(id, created, "date") values (:data_id, current_timestamp,
                to_char(current_timestamp, 'YYYY-MM-DD'))''', [i])

    oraclexe.commit()
    cursor.execute('select count(1) from data_append')
    assert cursor.fetchone()[0] == 20

    conn = conn_from_args(spark_session, args)
    df = conn.load()
    assert df.count() == 20

    ingest_count = incremental_append_ingestion(
        spark, df, db, tbl,
            args.incremental_column,
            args.last_value,
            args.storageformat,
            ingestion_tag_column=args.ingestion_tag_column,
            output_partitions=output_partitions)

    assert ingest_count == 10
    assert spark.sql('select count(1) as c from data_append').collect()[0][0] == 20


