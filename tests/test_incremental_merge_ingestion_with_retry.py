import sys
import subprocess
import pyspark
from getpass import getuser
from spark_dataloader.rdbms_ingestion import incremental_merge_ingestion_with_retry, Args, conn_from_args
import shutil
import glob
import os
import time

def test_incremental_merge_ingestion(mysql, spark_session):
    spark = spark_session
    raw_dbmeta = spark.sql('describe database default').collect()
    dbmeta = {}
    for i in raw_dbmeta:
        dbmeta[i[0]] = i[1]
    spark.sql('drop table if exists data_merge purge')
    spark.sql('drop table if exists data_merge_incremental purge')
    dblocation = dbmeta['Location'][5:]
    if os.path.exists(dblocation):
        shutil.rmtree(dblocation)

    args = Args(
            jdbc='jdbc:mysql://localhost:3600/ingestion',
            driver='com.mysql.jdbc.Driver',
            username='testuser',
            password='password',
            dbtable='ingestion.data_merge',
            partition_column='id',
            output_partition='date',
            incremental_column='id',
            storageformat='parquet',
            last_modified_column='last_modified',
            deleted_column='deleted',
            scrath_db='spark_scratch',
            key_columns='id',
            ingestion_tag_column='dl_ingest_date',
            consistency_retry=3,
            consistency_threshold=0,
            consistency_retry_delay=5,
            num_partitions='2')


    # populate database
    cursor = mysql.cursor()
    cursor.execute("create user if not exists testuser@localhost identified by 'password'")
    cursor.execute("grant all privileges on ingestion.* to testuser@localhost")

    cursor.execute('''
        create table data_merge(
            id int, value varchar(30), last_modified datetime, created datetime,
            `date` varchar(10), deleted int);''')


    for i in range(10):
        cursor.execute('''
            insert into data_merge(id, value, last_modified, created, `date`) values (%s,
            %s, now(), now(), date_format(now(), '%%Y-%%m-%%d'))''', [i, chr(ord('A') + i)])


    mysql.commit()

    cursor.execute('select count(1) from data_merge')
    assert cursor.fetchone()[0] == 10

    conn = conn_from_args(spark_session, args)
    df = conn.load()
    output_partitions = []
    if args.output_partition_columns:
        output_partitions = args.output_partition_columns.split(',')

    db = 'default'
    tbl = 'data_merge'


    ingest_count = incremental_merge_ingestion_with_retry(args, spark, df, db, tbl,
            args.key_columns.split(','),
            args.last_modified_column,
            last_modified=args.last_modified,
            incremental_column=args.incremental_column,
            last_value=args.last_value,
            deleted_column=args.deleted_column,
            scratch_db=args.scratch_db,
            storageformat=args.storageformat,
            ingestion_tag_column=args.ingestion_tag_column,
            output_partitions=output_partitions)

    assert ingest_count == 10
    assert spark.sql('select count(1) as rowc from data_merge').collect()[0].rowc == 10

    cursor.execute(r'''
    insert into data_merge(id, value, last_modified, created, `date`) values
    (11,'L',null,now(), date_format(now(), '%Y-%m-%d'))''')


    mysql.commit()
    cursor.execute('select count(1) from data_merge')
    assert cursor.fetchone()[0] == 11

    conn = conn_from_args(spark_session, args)
    df = conn.load()
    assert df.count() == 11

    ingest_count = incremental_merge_ingestion_with_retry(args, spark, df, db, tbl,
            args.key_columns.split(','),
            args.last_modified_column,
            last_modified=args.last_modified,
            incremental_column=args.incremental_column,
            last_value=args.last_value,
            deleted_column=args.deleted_column,
            scratch_db=args.scratch_db,
            storageformat=args.storageformat,
            ingestion_tag_column=args.ingestion_tag_column,
            output_partitions=output_partitions)



    assert ingest_count == 1
    assert spark.sql('select count(1) as c from data_merge').collect()[0][0] == 11

    cursor.execute('''
            update data_merge set last_modified=now(),value='K' where id=3''')
    mysql.commit()
    cursor.execute('select count(1) from data_merge')
    assert cursor.fetchone()[0] == 11

    conn = conn_from_args(spark_session, args)
    df = conn.load().cache()
    assert df.count() == 11


    cursor.execute('''
            update data_merge set last_modified=now(),value='J' where id=4''')
    mysql.commit()
    cursor.execute('select count(1) from data_merge')
    assert cursor.fetchone()[0] == 11

    ingest_count = incremental_merge_ingestion_with_retry(args, spark, df, db, tbl,
            args.key_columns.split(','),
            args.last_modified_column,
            last_modified=args.last_modified,
            incremental_column=args.incremental_column,
            last_value=args.last_value,
            deleted_column=args.deleted_column,
            scratch_db=args.scratch_db,
            storageformat=args.storageformat,
            ingestion_tag_column=args.ingestion_tag_column,
            output_partitions=output_partitions)

    assert ingest_count == 2
    assert spark.sql('select count(1) as c from data_merge').collect()[0][0] == 11
    assert spark.sql('select value from data_merge where id="3"').collect()[0][0] == 'K'
    assert spark.sql('select value from data_merge where id="4"').collect()[0][0] == 'J'

    ingest_count = incremental_merge_ingestion_with_retry(args, spark, df, db, tbl,
            args.key_columns.split(','),
            args.last_modified_column,
            last_modified=args.last_modified,
            incremental_column=args.incremental_column,
            last_value=args.last_value,
            deleted_column=args.deleted_column,
            scratch_db=args.scratch_db,
            storageformat=args.storageformat,
            ingestion_tag_column=args.ingestion_tag_column,
            output_partitions=output_partitions)

    assert ingest_count == 0

