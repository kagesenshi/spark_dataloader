import sys
import subprocess
import pyspark
from getpass import getuser
from spark_dataloader.rdbms_ingestion import incremental_merge_ingestion, Args, conn_from_args
import shutil
import glob
import os
import time

def test_incremental_merge_ingestion(mysql, spark_session):
    spark = spark_session
    raw_dbmeta = spark.sql('describe database default').collect()
    dbmeta = {}
    for i in raw_dbmeta:
        dbmeta[i[0]] = i[1]
    spark.sql('drop table if exists data_merge purge')
    spark.sql('drop table if exists data_merge_incremental purge')
    dblocation = dbmeta['Location'][5:]
    if os.path.exists(dblocation):
        shutil.rmtree(dblocation)

    args = Args(
            jdbc='jdbc:mysql://localhost:3600/ingestion',
            driver='com.mysql.jdbc.Driver',
            username='testuser',
            password='password',
            dbtable='ingestion.data_merge',
            partition_column='id',
            output_partition='date',
            incremental_column='id',
            storageformat='parquet',
            last_modified_column='last_modified',
            deleted_column='deleted',
            scrath_db='spark_scratch',
            key_columns='id',
            ingestion_tag_column='dl_ingest_date',
            num_partitions='2')


    # populate database
    cursor = mysql.cursor()
    cursor.execute("create user if not exists testuser@localhost identified by 'password'")
    cursor.execute("grant all privileges on ingestion.* to testuser@localhost")

    cursor.execute('''
        create table data_merge(
            id int, value varchar(30), last_modified datetime, created datetime,
            `date` varchar(10), deleted int);''')


    for i in range(10):
        cursor.execute('''
            insert into data_merge(id, value, last_modified, created, `date`) values (%s,
            %s, now(), now(), date_format(now(), '%%Y-%%m-%%d'))''', [i, chr(ord('A') + i)])


    mysql.commit()

    cursor.execute('select count(1) from data_merge')
    assert cursor.fetchone()[0] == 10

    conn = conn_from_args(spark_session, args)
    df = conn.load()
    output_partitions = []
    if args.output_partition_columns:
        output_partitions = args.output_partition_columns.split(',')

    db = 'default'
    tbl = 'data_merge'


    ingest_count = incremental_merge_ingestion(spark, df, db, tbl,
            args.key_columns.split(','),
            args.last_modified_column,
            args.last_modified,
            args.incremental_column,
            args.last_value,
            args.deleted_column,
            args.scratch_db,
            args.storageformat,
            ingestion_tag_column=args.ingestion_tag_column,
            output_partitions=output_partitions)

    assert ingest_count == 10
    assert spark.sql('select count(1) as rowc from data_merge').collect()[0].rowc == 10

    cursor.execute(r'''
    insert into data_merge(id, value, last_modified, created, `date`) values
    (11,'L',null,now(), date_format(now(), '%Y-%m-%d'))''')


    mysql.commit()
    cursor.execute('select count(1) from data_merge')
    assert cursor.fetchone()[0] == 11

    conn = conn_from_args(spark_session, args)
    df = conn.load()
    assert df.count() == 11

    ingest_count = incremental_merge_ingestion(spark, df, db, tbl,
            args.key_columns.split(','),
            args.last_modified_column,
            args.last_modified,
            args.incremental_column,
            args.last_value,
            args.deleted_column,
            args.scratch_db,
            args.storageformat,
            ingestion_tag_column=args.ingestion_tag_column,
            output_partitions=output_partitions)

    assert ingest_count == 1
    assert spark.sql('select count(1) as c from data_merge').collect()[0][0] == 11

    cursor.execute('''
            update data_merge set last_modified=now(),value='K' where id=3''')
    mysql.commit()
    cursor.execute('select count(1) from data_merge')
    assert cursor.fetchone()[0] == 11

    ingest_count = incremental_merge_ingestion(spark, df, db, tbl,
            args.key_columns.split(','),
            args.last_modified_column,
            args.last_modified,
            args.incremental_column,
            args.last_value,
            args.deleted_column,
            args.scratch_db,
            args.storageformat,
            ingestion_tag_column=args.ingestion_tag_column,
            output_partitions=output_partitions)

    assert ingest_count == 1
    assert spark.sql('select count(1) as c from data_merge').collect()[0][0] == 11
    assert spark.sql('select value from data_merge where id="3"').collect()[0][0] == 'K'




    # test if new column added in datasource 
    cursor.execute("alter table data_merge add newcol varchar(30) after value")

    for i in range(12, 22):
        cursor.execute('''
            insert into data_merge(id, value, newcol, last_modified, created, `date`) values (%s,
            %s, %s, now(), now(), date_format(now(), '%%Y-%%m-%%d'))''', 
            [i, chr(ord('A') + i), 'newcol%s' % i ])
    mysql.commit()
    
    cursor.execute('select count(1) from data_merge')
    res = cursor.fetchone()
    assert res[0] == 21

    conn = conn_from_args(spark_session, args)
    df = conn.load()

    spark.sql('alter table data_merge add columns (newcol string)')
    spark.sql('alter table data_merge_incremental add columns (newcol string)')

    newcol_distinct = spark.sql('select distinct newcol from data_merge').collect()

    assert len(newcol_distinct) == 1
    assert newcol_distinct[0].newcol is None

    ingest_count = incremental_merge_ingestion(spark, df, db, tbl,
            args.key_columns.split(','),
            args.last_modified_column,
            args.last_modified,
            args.incremental_column,
            args.last_value,
            args.deleted_column,
            args.scratch_db,
            args.storageformat,
            ingestion_tag_column=args.ingestion_tag_column,
            output_partitions=output_partitions)

    assert ingest_count == 10

    newcol_data = spark.sql('select newcol from data_merge').collect()

    for i in newcol_data:
        if i.newcol is None:
            continue
        assert i.newcol.startswith('newcol')


