#!/usr/bin/env python

import pytest
from pytest_mysql import factories
from getpass import getuser
import os
import findspark
import pytest
from pytest import FixtureRequest, TempPathFactory
import mirakuru
import subprocess
import oracledb
import oracledb.exceptions as oraexc
import pymssql
import time

os.environ['JAVA_HOME'] = '/usr/lib/jvm/jre-1.8.0-openjdk/'

mysql_proc = factories.mysql_proc(
        mysqld_exec='/usr/libexec/mysqld',
    port=3600, user=getuser())
mysql = factories.mysql('mysql_proc', dbname='ingestion')

@pytest.fixture
def oraclexe(request: FixtureRequest):
    conn = oracledb.connect(user='system', password='password', dsn='127.0.0.1/XEPDB1')
    yield conn
    conn.close()

@pytest.fixture
def mssqlxe(request: FixtureRequest):
    saconn = pymssql.connect('localhost', 'SA', 'Passw0rd')
    saconn.autocommit(True)
    with saconn.cursor() as cursor:
        cursor.execute('''IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'testdb')
BEGIN
  CREATE DATABASE testdb;
END
''')
    saconn.autocommit(False)

    conn = pymssql.connect('localhost', 'SA', 'Passw0rd', 'testdb')
    yield conn
    conn.close()
