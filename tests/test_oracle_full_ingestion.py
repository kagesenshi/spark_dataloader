import sys
import subprocess
import pyspark
from getpass import getuser
from spark_dataloader.rdbms_ingestion import full_ingestion, Args, conn_from_args
import shutil
import glob
import os

def test_full_ingestion(oraclexe, spark_session):
    spark = spark_session

    raw_dbmeta = spark.sql('describe database default').collect()
    dbmeta = {}
    for i in raw_dbmeta:
        dbmeta[i[0]] = i[1]
    spark.sql('drop table if exists data')
    dblocation = dbmeta['Location'][5:]
    if os.path.exists(dblocation):
        shutil.rmtree(dblocation)

    # populate database
    cursor = oraclexe.cursor()
    cursor.execute("""
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE DATA';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
                   """)
    cursor.execute("create table data(id integer, value varchar(30), created timestamp)")
    for i in range(100):
        cursor.execute('insert into data(id, value, created) values (:data_id, :data_value, current_timestamp)', [i, 'data%s' % i])
    oraclexe.commit()
    cursor.execute('select count(1) from data')

    res = cursor.fetchone()

    assert res[0] == 100

    args = Args(
            jdbc='jdbc:oracle:thin:@localhost:1521/XEPDB1',
            driver='oracle.jdbc.driver.OracleDriver',
            username='system',
            password='password',
            dbtable='data',
            ingestion_tag_column='dl_ingest_date',
            partition_column='id',
            storageformat='parquet',
            num_partitions='2')

    conn = conn_from_args(spark_session, args)
    df = conn.load()
    output_partitions = []
    if args.output_partition_columns:
        output_partitions = args.output_partition_columns.split(',')

    db = 'default'
    tbl = 'data'

    ingest_count = full_ingestion(spark_session, df, db, tbl, args.overwrite,
            args.storageformat, ingestion_tag_column=args.ingestion_tag_column,
            output_partitions=output_partitions)

    assert ingest_count == 100

    assert spark.sql('select count(1) as rowc from data').collect()[0].rowc == 100

    raw_metadata = spark.sql('desc formatted data').collect()
    metadata = {}
    for i in raw_metadata:
        metadata[i.col_name] = i.data_type
    location = metadata['Location'][5:]

    assert len(glob.glob(os.path.join(location, '*.parquet'))) == 2

