==================
Spark Data Loaders
==================


.. image:: https://img.shields.io/pypi/v/spark_dataloader.svg
        :target: https://pypi.python.org/pypi/spark_dataloader

.. image:: https://img.shields.io/travis/kagesenshi/spark_dataloader.svg
        :target: https://travis-ci.com/kagesenshi/spark_dataloader

.. image:: https://readthedocs.org/projects/spark-dataloader/badge/?version=latest
        :target: https://spark-dataloader.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Standard data loaders for Apache Spark


* Free software: Apache Software License 2.0
* Documentation: https://spark-dataloader.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
