=======
History
=======

0.1.4 (unreleased)
------------------

- Nothing changed yet.


0.1.3 (2022-07-31)
------------------

- Fixed config file support for incremental append & merge


0.1.2 (2022-07-28)
------------------

- Fix cli module inclusion


0.1.1 (2022-07-28)
------------------

- Added config file support


0.1.0 (2022-07-04)
------------------

* First release on PyPI.
